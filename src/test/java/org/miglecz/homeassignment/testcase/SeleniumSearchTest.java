package org.miglecz.homeassignment.testcase;

import java.util.List;
import org.miglecz.homeassignment.domain.SearchResult;
import org.miglecz.homeassignment.domain.SearchResultPage;
import org.miglecz.homeassignment.framework.TestBase;
import org.testng.annotations.Test;
import static com.google.common.truth.Truth.assertThat;
import static java.lang.System.lineSeparator;
import static java.util.stream.Collectors.joining;
import static org.miglecz.homeassignment.framework.ListHelper.firstItem;
import static org.miglecz.homeassignment.framework.ListHelper.lastItem;

public class SeleniumSearchTest extends TestBase {
    @Test
    void searchShouldFindResults() {
        // Given
        final SearchResultPage page = firefoxBrowser()
            .startPage()
            .rejectCookies() //this may be included in the previous method
            .type("Java")
            .googleSearch();
        // When
        final List<SearchResult> results = page.getResults();
        // Then
        System.out.println(
            results.stream()
                //.map(SearchResult::getTitle)
                //.map(SearchResult::getBreadcrumb)
                .map(SearchResult::getLink)
                .collect(joining(lineSeparator()))
        );
        assertThat(results)
            .isNotEmpty();
        assertThat(firstItem(results).getTitle())
            .contains("Java");
        assertThat(lastItem(results).getTitle())
            .doesNotContain("Interview");
    }
}
