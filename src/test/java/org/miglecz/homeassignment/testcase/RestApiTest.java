package org.miglecz.homeassignment.testcase;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import lombok.Builder;
import org.miglecz.homeassignment.framework.TestBase;
import org.testng.annotations.Test;
import static com.google.common.truth.Truth.assertThat;
import static java.lang.System.lineSeparator;
import static java.util.stream.Collectors.joining;

public class RestApiTest extends TestBase {
    @Test
    void restApiShouldReturnCategoryOfAuthenticationAndAuthorization() {
        // Given
        final String url = "https://api.publicapis.org/entries";
        // When
        final Response response = RestAssured.when()
            .get(url);
        // Then
        final List<Map<String, Object>> actualResult = response
            .then()
            .statusCode(200)
            .extract()
            .jsonPath()
            .getList("entries.findAll {e -> e.Category == 'Authentication & Authorization'}");
        assertThat(actualResult)
            .containsExactly(
                expectedItems()
            );
        System.out.println(
            actualResult.stream()
                .map(Object::toString)
                .collect(joining(lineSeparator()))
        );
    }

    private Object[] expectedItems() {
        return Stream.of(
                Item.builder()
                    .api("Auth0")
                    .description("Easy to implement, adaptable authentication and authorization platform")
                    .cors("yes")
                    .link("https://auth0.com")
                    .build()
                , Item.builder()
                    .api("GetOTP")
                    .description("Implement OTP flow quickly")
                    .cors("no")
                    .link("https://otp.dev/en/docs/")
                    .build()
                , Item.builder()
                    .api("Micro User Service")
                    .description("User management and authentication")
                    .cors("no")
                    .link("https://m3o.com/user")
                    .build()
                , Item.builder()
                    .api("MojoAuth")
                    .description("Secure and modern passwordless authentication platform")
                    .cors("yes")
                    .link("https://mojoauth.com")
                    .build()
                , Item.builder()
                    .api("SAWO Labs")
                    .description("Simplify login and improve user experience by integrating passwordless authentication in your app")
                    .cors("yes")
                    .link("https://sawolabs.com")
                    .build()
                , Item.builder()
                    .api("Stytch")
                    .description("User infrastructure for modern applications")
                    .cors("no")
                    .link("https://stytch.com/")
                    .build()
                , Item.builder()
                    .api("Warrant")
                    .description("APIs for authorization and access control")
                    .cors("yes")
                    .link("https://warrant.dev/")
                    .build()
            )
            .map(this::toMap)
            .toArray();
    }

    private LinkedHashMap<String, Object> toMap(final Item item) {
        return new LinkedHashMap<String, Object>() {{
            put("API", item.api);
            put("Description", item.description);
            put("Auth", "apiKey");
            put("HTTPS", true);
            put("Cors", item.cors);
            put("Link", item.link);
            put("Category", "Authentication & Authorization");
        }};
    }

    @Builder
    static class Item {
        String api, description, cors, link;
    }
}
