package org.miglecz.homeassignment.framework;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import static java.util.Collections.emptyList;

/**
 * Providing WebElement find methods. Expecting no implicit WebDriver wait to be configured.
 * Methods name explanation: <find><wait>element<s>
 * element - return single WebElement
 * elements - return list of WebElement
 * find prefix - return finding or throw NoSuchElementException
 * wait prefix - retry until timeout
 */
public class Search {
    private final SearchContext context;

    private Search(final SearchContext context) {
        this.context = context;
    }

    public static Search of(final SearchContext context) {
        return new Search(context);
    }

    public WebElement element(final By by) {
        try {
            return context()
                .findElement(by);
        } catch (final NoSuchElementException ignore) {
            return null;
        }
    }

    public List<WebElement> elements(final By by) {
        throw error();
    }

    public WebElement findElement(final By by) {
        return context().findElement(by);
    }

    public List<WebElement> findElements(final By by) {
        throw error();
    }

    public WebElement waitElement(final By by) {
        try {
            return fluentWait(by)
                .until(locator -> context()
                    .findElement(locator)
                );
        } catch (final TimeoutException ignore) {
            return null;
        }
    }

    public List<WebElement> waitElements(final By by) {
        try {
            return fluentWait(by)
                .until(locator -> {
                    final List<WebElement> list = context()
                        .findElements(locator);
                    if (list.isEmpty()) {
                        return null;
                    }
                    return list;
                });
        } catch (final TimeoutException ignore) {
            return emptyList();
        }
    }

    public WebElement findWaitElement(final By by) {
        return fluentWait(by)
            .until(locator -> context()
                .findElement(locator)
            );
    }

    public List<WebElement> findWaitElements(final By by) {
        throw error();
    }

    private SearchContext context() {
        return context;
    }

    private <T> FluentWait<T> fluentWait(final T t) {
        return FluentWaitFactory.fluentWait(t);
    }

    @Deprecated
    private UnsupportedOperationException error() {
        return new UnsupportedOperationException("not implemented yet");
    }
}
