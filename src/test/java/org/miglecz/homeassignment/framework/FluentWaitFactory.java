package org.miglecz.homeassignment.framework;

import java.time.Duration;
import lombok.experimental.UtilityClass;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.FluentWait;

@UtilityClass
public class FluentWaitFactory {
    public static <T> FluentWait<T> fluentWait(final T t) {
        return new FluentWait<>(t)
            .ignoring(WebDriverException.class)
            .withTimeout(Duration.ofSeconds(5))
            .pollingEvery(Duration.ofMillis(100));
    }
}
