package org.miglecz.homeassignment.framework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.SneakyThrows;
import org.miglecz.homeassignment.domain.EmptyPage;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class TestBase {
    private final ThreadLocal<List<WebDriver>> drivers = ThreadLocal.withInitial(ArrayList::new);
    private static final Map<Integer, String> STATUS_MAP = new HashMap<Integer, String>() {{
        /** Source: {@link ITestResult#getStatus()} */
        put(-1, "CREATED");
        put(1, "SUCCESS");
        put(2, "FAILURE");
        put(3, "SKIP");
        put(4, "SUCCESS_PERCENTAGE_FAILURE");
        put(16, "STARTED");
    }};

    protected EmptyPage firefoxBrowser() {
        final WebDriver driver = DriverFactory.firefoxBrowser();
        drivers.get()
            .add(driver);
        setupDriver(driver);
        return new EmptyPage(driver);
    }

    private static void setupDriver(final WebDriver driver) {
        driver.manage()
            .window()
            .maximize();
    }

    @BeforeMethod
    public void beforeMethod(final ITestResult result) {
        System.out.printf("%n%s.%s starting%n"
            , result.getMethod().getRealClass().getSimpleName()
            , result.getMethod().getMethodName()
        );
    }

    @AfterMethod
    public void afterMethod(final ITestResult result) {
        System.out.printf("%s.%s ended: %s%n"
            , result.getMethod().getRealClass().getSimpleName()
            , result.getMethod().getMethodName()
            , STATUS_MAP.get(result.getStatus())
        );
        drivers.get().forEach(WebDriver::quit);
        drivers.get().clear();
    }

    @Deprecated
    @SneakyThrows
    protected void sleep() {
        Thread.sleep(2000);
    }
}
