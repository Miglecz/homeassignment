package org.miglecz.homeassignment.framework;

import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.experimental.UtilityClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

@UtilityClass
public class DriverFactory {
    public static WebDriver firefoxBrowser() {
        WebDriverManager.firefoxdriver().setup();
        return new FirefoxDriver(new FirefoxOptions() {{
            addArguments("-headless");
        }});
    }
}
