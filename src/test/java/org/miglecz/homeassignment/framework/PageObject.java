package org.miglecz.homeassignment.framework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.FluentWait;

/**
 * Baseclass for pages.
 */
public class PageObject {
    private final WebDriver driver;
    private final Search search;

    /**
     * This is for initial page only to be used only once.
     */
    public PageObject(final WebDriver driver) {
        this.driver = driver;
        search = Search.of(driver);
    }

    public PageObject(final PageObject parent) {
        driver = parent.driver;
        search = Search.of(driver);
    }

    /**
     * try not to use this too much
     */
    @Deprecated
    protected WebDriver driver() {
        return driver;
    }

    public Search search() {
        return search;
    }

    protected <T> FluentWait<T> fluentWait(final T t) {
        return FluentWaitFactory.fluentWait(t);
    }
}
