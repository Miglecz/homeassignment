package org.miglecz.homeassignment.framework;

import java.util.List;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ListHelper {
    public static <T> T firstItem(final List<T> results) {
        return results.get(0);
    }

    public static <T> T lastItem(final List<T> list) {
        return list.get(list.size() - 1);
    }
}
