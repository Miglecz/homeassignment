package org.miglecz.homeassignment.domain;

import java.util.List;
import org.miglecz.homeassignment.framework.PageObject;
import org.miglecz.homeassignment.framework.Search;
import org.openqa.selenium.By;
import static java.util.stream.Collectors.toList;

public class SearchResultPage extends PageObject {
    public static final By CONTAINER_RESULT = By.xpath("(//*[@id='rso']/*)[position()!=3]"
    );
    public static final By TEXT_BREADCRUMB = By.xpath(".//cite");
    public static final By TEXT_TITLE = By.xpath(".//h3");
    public static final By TEXT_URL = By.xpath("(.//a)[1]");

    public SearchResultPage(final PageObject parent) {
        super(parent);
    }

    public List<SearchResult> getResults() {
        return search()
            .waitElements(CONTAINER_RESULT)
            .stream()
            .map(element -> {
                final Search search = Search.of(element);
                return new SearchResult(
                    search.findWaitElement(TEXT_BREADCRUMB)
                        .getText(),
                    search.findWaitElement(TEXT_TITLE)
                        .getText(),
                    search.findWaitElement(TEXT_URL)
                        .getAttribute("href")
                );
            })
            .collect(toList());
    }
}
