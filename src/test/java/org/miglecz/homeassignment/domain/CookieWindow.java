package org.miglecz.homeassignment.domain;

import org.miglecz.homeassignment.framework.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import static java.util.Optional.ofNullable;

public class CookieWindow extends PageObject {
    private static final By BUTTON_COOKIE_REJECT = By.xpath("(//button)[3]");

    public CookieWindow(final PageObject parent) {
        super(parent);
    }

    public StartPage rejectCookies() {
        ofNullable(search()
            .waitElement(BUTTON_COOKIE_REJECT))
            .ifPresent(WebElement::click);
        return new StartPage(this);
    }
}
