package org.miglecz.homeassignment.domain;

import org.miglecz.homeassignment.framework.PageObject;
import org.openqa.selenium.By;

public class StartPage extends PageObject {
    private static final By TEXT_SEARCH = By.name("q");
    private static final By BUTTON_SEARCH = By.name("btnK");

    public StartPage(final PageObject parent) {
        super(parent);
    }

    public StartPage type(final String keys) {
        driver()
            .findElement(TEXT_SEARCH)
            .sendKeys(keys);
        return this;
    }

    public SearchResultPage googleSearch() {
        fluentWait(BUTTON_SEARCH)
            .until(by -> {
                search()
                    .findElement(by)
                    .click();
                return true;
            });
        return new SearchResultPage(this);
    }
}
