package org.miglecz.homeassignment.domain;

import org.miglecz.homeassignment.framework.PageObject;
import org.openqa.selenium.WebDriver;

public class EmptyPage extends PageObject {
    public EmptyPage(final WebDriver driver) {
        super(driver);
    }

    public CookieWindow startPage() {
        driver().get("https://www.google.co.il/");
        return new CookieWindow(this);
    }
}
