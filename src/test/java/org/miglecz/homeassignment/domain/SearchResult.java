package org.miglecz.homeassignment.domain;

import lombok.Value;

@Value
public class SearchResult {
    String breadcrumb;
    String title;
    String link;
}
