# Home Assignment

Java module featuring WebBrowser automation and REST API testing.

## Requirements
- JDK 8 compilation (note: however some dependency binaries are JDK11 version bytecode)
- Apache Maven 3.8.4
- Internet connection for accessing [https://www.google.co.il/](https://www.google.co.il/) and [https://api.publicapis.org/entries](https://api.publicapis.org/entries)
- Preinstalled latest Firefox browser

## Features

1. TestNG test
	1. Open browser (any browser that you want)
	1. Go to [https://www.google.co.il/](https://www.google.co.il/)
	1. Type “Java” into search field
	1. Press on “Google Search” button
	1. Print only search result links(without internal references* _see pic. below_)
	1. Verify first search result contains the word “Java”
	1. Verify last search result doesn’t contain the word “Interview”

1. RESTful API test
	1. Online free endpoint -[https://api.publicapis.org/entries](https://api.publicapis.org/entries)
	1. Call this API endpoint
	1. Read the response, find all objects with property **_“Category: Authentication & Authorization”_**
	1. Compare, count, and verify the number of objects where the property above appears
	1. Finally print found objects in console

1. Gitlab public project
	1. Acquire shared docker runner (i.e., use one of the default publics GitLab allows)
	1. Create .gitlab-ci.yml file in project
	1. Make sure CI/CD pipeline works
	1. Show tests run in a pipeline in CI/CD of the Gitlab project.

## License
[GPLv3](https://www.gnu.org/licenses/quick-guide-gplv3.html)
